# -*- coding: utf-8 -*-


# Returns symbols used for printing board
def symbol(tile):
    if tile == "E":
        return " "
    elif tile == "W":
        return u"\u25CF"
    elif tile == "w":
        return u"\u25A0"
    elif tile == "B":
        return u"\u25CB"
    elif tile == "b":
        return u"\u25A5"


# Displays the board to the user
def print_board(board):
    print('\n')
    line = "X |"
    letters = ["A", "B", "C", "D", "E", "F", "G", "H"]
    for i in range(8):
        line += " " + letters[i] + " |"
    print(line)
    for j in range(8):
        line = str(j) + " |"
        for k in range(8):
            line += " " + symbol(str(board[j][k])) + " |"
        print(line)
    print('\n')


# Used when moving a piece, checks move will not take piece off board
def on_board(coord, direction):
    if direction[0] == "u":
        if coord == 0:
            raise (Exception("Off the board"))
        else:
            coord -= 1
    elif direction[0] == "r":
        if coord == 7:
            raise (Exception("Off the board"))
        else:
            coord += 1
    elif direction[0] == "l":
        if coord == 0:
            raise (Exception("Off the board"))
        else:
            coord -= 1
    elif direction[0] == "d":
        if coord == 7:
            raise (Exception("Off the board"))
        else:
            coord += 1
    return coord


# In future this would be used to export the sequence of moves using the notation for English Draughts
def coord_to_number(y_coord, x_coord):
    if y_coord % 2 == 0:
        return (y_coord * 8 + x_coord + 1) / 2
    else:
        return (y_coord * 8 + x_coord + 2) / 2


# Function for moving pieces after user's input
def simple_move(moves, board, color, y_coord, x_coord, y_direction, x_direction):
    piece = board[y_coord][x_coord]
    old_x = x_coord
    old_y = y_coord
    if piece.capitalize() == color:
        if piece.isupper():
            if color == "B" and y_direction == "up":
                raise (Exception("Wrong direction"))
            elif color == "W" and y_direction == "down":
                raise (Exception("Wrong direction"))
        y_coord = on_board(y_coord, y_direction)
        x_coord = on_board(x_coord, x_direction)
        if board[y_coord][x_coord] == "E":
            moves.append({'turn': color, 'start': [old_y, old_x], 'end': [y_coord, x_coord]})
            board[y_coord][x_coord] = piece
            board[old_y][old_x] = "E"
            # If piece is at top/bottom of board it becomes a king
            if y_coord == 0 and color == "W":
                board[y_coord][x_coord] = str.lower(color)
            elif y_coord == 7 and color == "B":
                board[y_coord][x_coord] = str.lower(color)
        elif board[y_coord][x_coord] == color:
            raise (Exception("Space occupied - " + board[y_coord][x_coord]))
        else:
            # If the space being moved into is occupied check if it can be jumped, else return an error
            if len(path(board, color, old_y, old_x, y_direction, x_direction, [])) > 1:
                jump(moves, board, color, old_y, old_x, y_direction, x_direction)
            else:
                raise (Exception("Space occupied - " + board[y_coord][x_coord]))

    else:
        raise (Exception("Wrong color"))


# Mainly used to check if a square is occupied by the opposition's piece
def opposite(word):
    words = ["up", "down", "left", "right", "B", "W"]
    opposites = ["down", "up", "right", "left", "W", "B"]
    if word in words:
        return opposites[words.index(word)]
    else:
        return False


# Basic computer player
def computer_player(moves, board):
    for y_coord, row in enumerate(board):
        for x_coord, piece in enumerate(row):
            try:
                if piece == "B":
                    simple_move(moves, board, "B", y_coord, x_coord, "down", "left")
                    return True
            except Exception as error:
                # No legal move in that direction, try the other
                try:
                    if piece == "B":
                        simple_move(moves, board, "B", y_coord, x_coord, "down", "right")
                        return True
                except Exception as error:
                    # Try the next piece on the board
                    pass


# Used to find path piece will take when taking other pieces
def path(board, color, y_coord, x_coord, y_direction, x_direction, found_pieces):
    found_path = []
    if color == "W":
        if len(found_pieces) > 0 and found_pieces[-1] == opposite(color):
            if board[y_coord][x_coord] == opposite(color):
                if y_coord > 0 and x_coord > 0 and board[y_coord - 1][x_coord - 1] == "E":
                    found_path.append([y_coord - 1, x_coord - 1])
                    found_pieces.append("E")
                elif y_coord > 0 and x_coord < 7 and board[y_coord - 1][x_coord + 1] == "E":
                    found_path.append([y_coord - 1, x_coord + 1])
                    found_pieces.append("E")
        elif len(found_pieces) > 0 and found_pieces[-1] == "E":
            if board[y_coord][x_coord] == "E":
                if y_coord > 0 and x_coord > 0 and board[y_coord - 1][x_coord - 1] == opposite(color):
                    if found_pieces and found_pieces[-1] == "E":
                        found_pieces.append(opposite(color))
                        found_path.append([y_coord - 1, x_coord - 1])
                    elif len(found_pieces) == 0:
                        found_pieces.append(opposite(color))
                        found_path.append([y_coord - 1, x_coord - 1])
                    if y_coord > 1 and x_coord > 1:
                        found_path += path(board, color, y_coord - 1, x_coord - 1, y_direction, x_direction,
                                           found_pieces)
                    elif y_coord > 1:
                        found_path += path(board, color, y_coord - 1, x_coord + 1, y_direction, x_direction,
                                           found_pieces)
                elif y_coord > 0 and x_coord < 7 and board[y_coord - 1][x_coord + 1] == opposite(color):
                    found_pieces.append(opposite(color))
                    found_path.append([y_coord - 1, x_coord + 1])
                    if found_pieces and found_pieces[-1] == "E":
                        found_path.append([y_coord - 1, x_coord + 1])
                    elif len(found_pieces) == 0:
                        found_path.append([y_coord - 1, x_coord + 1])
                    if y_coord > 2:
                        found_path += path(board, color, y_coord - 1, x_coord + 1, y_direction, x_direction,
                                           found_pieces)
                    elif y_coord > 2 and x_coord != 6:
                        found_path += path(board, color, y_coord - 1, x_coord + 1, y_direction, x_direction,
                                           found_pieces)
        else:
            y_coord = on_board(y_coord, y_direction)
            x_coord = on_board(x_coord, x_direction)
            found_path.append([y_coord, x_coord])
            found_pieces.append(opposite(color))
            if y_coord > 0 and x_coord > 0 and board[y_coord - 1][x_coord - 1] == "E":
                found_path.append([y_coord - 1, x_coord - 1])
                found_pieces.append("E")
                found_path += path(board, color, y_coord - 1, x_coord - 1, y_direction, x_direction, found_pieces)
            elif y_coord > 0 and x_coord < 6 and board[y_coord - 1][x_coord + 1] == "E":
                found_path.append([y_coord - 1, x_coord + 1])
                found_pieces.append("E")
                found_path += path(board, color, y_coord - 1, x_coord + 1, y_direction, x_direction, found_pieces)
    elif color == "B":
        if len(found_pieces) > 0 and found_pieces[-1] == "E":
            if board[y_coord][x_coord] == opposite(color):
                # look for E
                if y_coord > 0 and x_coord > 0 and board[y_coord + 1][x_coord - 1] == "E":
                    found_path.append([y_coord + 1, x_coord - 1])
                    found_pieces.append("E")
                elif y_coord > 0 and x_coord < 7 and board[y_coord + 1][x_coord + 1] == "E":
                    found_path.append([y_coord + 1, x_coord + 1])
                    found_pieces.append("E")
        elif len(found_pieces) > 0 and found_pieces[-1] == opposite(color):
            if board[y_coord][x_coord] == "E":
                if y_coord > 0 and x_coord > 0 and board[y_coord + 1][x_coord - 1] == opposite(color):
                    if found_pieces and found_pieces[-1] == "E":
                        found_pieces.append(opposite(color))
                        found_path.append([y_coord + 1, x_coord - 1])
                    elif len(found_pieces) == 0:
                        found_pieces.append(opposite(color))
                        found_path.append([y_coord + 1, x_coord - 1])
                    if y_coord > 1 and x_coord > 1:
                        found_path += path(board, color, y_coord + 1, x_coord - 1, y_direction, x_direction,
                                           found_pieces)
                    elif y_coord > 1:
                        found_path += path(board, color, y_coord + 1, x_coord + 1, y_direction, x_direction,
                                           found_pieces)
                elif y_coord > 0 and x_coord < 7 and board[y_coord + 1][x_coord + 1] == opposite(color):
                    found_path += path(board, color, y_coord + 1, x_coord + 1, y_direction, x_direction, found_pieces)
                    if found_pieces and found_pieces[-1] == "E":
                        found_pieces.append(opposite(color))
                        found_path.append([y_coord + 1, x_coord + 1])
                    elif len(found_pieces) == 0:
                        found_pieces.append(opposite(color))
                        found_path.append([y_coord + 1, x_coord + 1])
                    if y_coord < 6:
                        found_path += path(board, color, y_coord + 1, x_coord + 1, y_direction, x_direction,
                                           found_pieces)
                    elif y_coord < 6 and x_coord != 6:
                        found_path += path(board, color, y_coord + 1, x_coord + 1, y_direction, x_direction,
                                           found_pieces)
        else:
            y_coord = on_board(y_coord, y_direction)
            x_coord = on_board(x_coord, x_direction)
            found_path.append([y_coord, x_coord])
            found_pieces.append(opposite(color))
            if y_coord < 6 and x_coord > 0 and board[y_coord + 1][x_coord - 1] == "E":
                found_path.append([y_coord + 1, x_coord - 1])
                found_pieces.append("E")
                found_path += path(board, color, y_coord + 1, x_coord - 1, y_direction, x_direction, found_pieces)
            elif y_coord < 6 and x_coord < 6 and board[y_coord + 1][x_coord + 1] == "E":
                found_path.append([y_coord + 1, x_coord + 1])
                found_pieces.append("E")
                found_path += path(board, color, y_coord + 1, x_coord + 1, y_direction, x_direction, found_pieces)
    return found_path


# Given a piece and a direction, computes path that the piece will take for all possible jumps
# then changes board accordingly
def jump(moves, board, color, y_coord, x_coord, y_direction, x_direction):
    old_x = x_coord
    old_y = y_coord
    board[old_y][old_x] = "E"
    pieces = path(board, color, y_coord, x_coord, y_direction, x_direction, [])
    taken = []
    for x in pieces:
        if x != pieces[-1]:
            board[x[0]][x[1]] = "E"
            taken.append([x[0], x[1]])
        else:
            board[x[0]][x[1]] = color
            new_y = x[0]
            new_x = x[1]
            moves.append(
                {'turn': color, 'start': [old_y, old_x], 'end': [x[0], x[1]], 'taken': taken})


# Gets input from user, converts to usable format
def get_piece(board, color):
    coords = input("Choose piece (e.g. C5 ) \n")
    letters = ["A", "B", "C", "D", "E", "F", "G", "H"]
    x_coord = int(letters.index(str.upper(coords)[0]))
    y_coord = int(coords[1])
    if board[y_coord][x_coord].capitalize() != color:
        raise (Exception("Wrong color"))
    return [y_coord, x_coord]


# Gets input from user, converts to usable format
def get_direction():
    direction = input("Which direction (e.g. up right)?\n")
    y_direction = direction.split()[0]
    x_direction = direction.split()[1]
    return [y_direction, x_direction]


# Counts pieces on board to check if game has been won
def game_won(board):
    black_counter = 0
    white_counter = 0
    for y_coord, row in enumerate(board):
        for x_coord, piece in enumerate(row):
            if piece == "B":
                black_counter += 1
            elif piece == "W":
                white_counter += 1
    if black_counter == 0:
        return "White won"
    elif white_counter == 0:
        return "Black won"
    else:
        return False


# Reverts board to state before previous move
def undo(board, moves, undo_count):
    move = moves[-1 - undo_count]
    board[int(move['start'][0])][int(move['start'][1])] = move['turn']
    board[int(move['end'][0])][int(move['end'][1])] = "E"
    if 'taken' in move:
        for piece in move['taken']:
            board[piece[0]][piece[1]] = opposite(move['turn'])
    return undo_count + 1


# Opposite of undo
def redo(board, moves, undo_count):
    move = moves[-1 * undo_count]
    board[int(move['start'][0])][int(move['start'][1])] = "E"
    board[int(move['end'][0])][int(move['end'][1])] = move['turn']
    if 'taken' in move:
        for piece in move['taken']:
            board[piece[0]][piece[1]] = "E"
    return undo_count - 1


# Given a board and the moves done on that board, replays all moves
def replay(board, moves):
    for move in moves:
        board[int(move['start'][0])][int(move['start'][1])] = "E"
        board[int(move['end'][0])][int(move['end'][1])] = move['turn']
        if 'taken' in move:
            for piece in move['taken']:
                board[piece[0]][piece[1]] = "E"
        print_board(board)


# Main program function
def main():
    board = [
        ["E", "B", "E", "B", "E", "B", "E", "B"],
        ["B", "E", "B", "E", "B", "E", "B", "E"],
        ["E", "B", "E", "B", "E", "B", "E", "B"],
        ["E", "E", "E", "E", "E", "E", "E", "E"],
        ["E", "E", "E", "E", "E", "E", "E", "E"],
        ["W", "E", "W", "E", "W", "E", "W", "E"],
        ["E", "W", "E", "W", "E", "W", "E", "W"],
        ["W", "E", "W", "E", "W", "E", "W", "E"]
    ]
    moves = []
    color = "B"
    undo_count = 0
    game_type = input("Hello\nWill you be playing one player or two player? \n1P/2P\n")
    if game_type.lower() != "1p" and game_type.lower() != "2p":
        game_type = input("1P/2P\n")
    while not game_won(board):
        turn_complete = False
        error_flag = False
        print_board(board)
        print(color + "'s turn")
        piece = []
        direction = []
        while not turn_complete and not error_flag:
            if game_type.lower() == "1p" and color == "B":
                computer_player(moves, board)
                color = opposite(color)
                turn_complete = True
            else:
                try:
                    option = input("Do you want to move, undo or redo? \n")
                    if option.lower() == "move":
                        if not piece:
                            piece = get_piece(board, color)
                        if not direction:
                            direction = get_direction()
                        simple_move(moves, board, color, piece[0], piece[1], direction[0], direction[1])
                        color = opposite(color)
                        turn_complete = True
                    elif option.lower() == "undo":
                        undo_count = undo(board, moves, undo_count)
                        print_board(board)
                        color = opposite(color)
                        turn_complete = False
                    elif option.lower() == "redo":
                        undo_count = redo(board, moves, undo_count)
                        print_board(board)
                        color = opposite(color)
                        turn_complete = False
                    else:
                        print("Input not recognised")
                except Exception as error:
                    print("ERROR", error)
                    # traceback.print_exc(file=sys.stdout)
                    error_flag = True
    print(game_won(board))
    while input("Replay game? (or quit)\n").lower() != "quit":
        board = [
            ["E", "B", "E", "B", "E", "B", "E", "B"],
            ["B", "E", "B", "E", "B", "E", "B", "E"],
            ["E", "B", "E", "B", "E", "B", "E", "B"],
            ["E", "E", "E", "E", "E", "E", "E", "E"],
            ["E", "E", "E", "E", "E", "E", "E", "E"],
            ["W", "E", "W", "E", "W", "E", "W", "E"],
            ["E", "W", "E", "W", "E", "W", "E", "W"],
            ["W", "E", "W", "E", "W", "E", "W", "E"]
        ]
        replay(board, moves)


if __name__ == "__main__":
    main()
